package fr.uavignon.ceri.tp2;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailViewModel extends AndroidViewModel {

    private BookRepository repository;
    private LiveData<List<Book>> allBooks;
    private MutableLiveData<List<Book>> selectedBook;


    public DetailViewModel(Application application) {
        super(application);
        repository   = new BookRepository(application);
        allBooks     = repository.getAllBooks();
        selectedBook = repository.getSelectedBook();
    }

    MutableLiveData<List<Book>> getSelectedBook() {
        return selectedBook;
    }

    LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public void insertBook(Book book) {
        repository.insertBook(book);
    }

    public void getBook(long id) {
        repository.getBook(id);
    }

    public void  insertOrUpdateBook(Book book) {
        /*f(selectedBook.getValue().get(0).getId() != -1) {
            repository.updateBook(book);
            Log.i("xxx", "this is update");
        }else {
            Log.i("xxx", "this is insert");
            insertBook(book);
        }*/



        Log.i("xxx", "this is insertOrUpdate");
        if(selectedBook.getValue() == null) {
            Log.i("xxx", "this is insert");
            repository.insertBook(book);
        }else {
            repository.updateBook(book);
            Log.i("xxx", "this is update");
        }

    }



    public void deleteBook(long id) {
        repository.deleteBook(id);
    }

}
