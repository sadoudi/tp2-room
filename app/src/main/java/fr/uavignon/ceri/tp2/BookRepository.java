package fr.uavignon.ceri.tp2;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {

    private MutableLiveData<List<Book>> selectedBook =
            new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;
    private BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
/*
        Log.d("wina", allBooks.getValue().toString());
*/
    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public MutableLiveData<List<Book>> getSelectedBook() {
        return selectedBook;
    }



    public void insertBook(Book newBook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newBook);
        });
    }

    public void updateBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(book);
        });
    }

    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }

    public void getBook(long id) {
        Future<List<Book>> fbooks = databaseWriteExecutor.submit(() -> {
           return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(fbooks.get());
        }catch (ExecutionException e) {
            e.printStackTrace();
        }catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

}

