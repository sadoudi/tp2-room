package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private DetailViewModel viewModel;
    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    private Book selectedBook;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        // Get selected book

        //Book book = Book.books[(int)args.getBookNum()];
        //Book book = viewModel.getBook((int) args.getBookNum());

        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        viewModel.getBook(args.getBookNum());

        listenerSetup();
        observerSetup();


    }

    private void listenerSetup() {
        Button updateButton = getView().findViewById(R.id.buttonUpdate);
        Button backButton   = getView().findViewById(R.id.buttonBack);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title     = textTitle.getText().toString();
                String authors   = textAuthors.getText().toString();
                String year      = textYear.getText().toString();
                String genres    = textGenres.getText().toString();
                String publisher = textPublisher.getText().toString();

                if(!title.equals("") && !authors.equals("")) {
                    // Book b = viewModel.getSelectedBook().getValue().get(0);
                    if(selectedBook == null) {
                        selectedBook = new Book();
                    }
                    selectedBook.setTitle(textTitle.getText().toString());
                    selectedBook.setAuthors(textAuthors.getText().toString());
                    selectedBook.setYear(textYear.getText().toString());
                    selectedBook.setGenres(textGenres.getText().toString());
                    selectedBook.setPublisher(textPublisher.getText().toString());

                    //DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
                    //Book updatedBook = new Book(args.getBookNum(), title, authors, year, genres, publisher);

                    viewModel.insertOrUpdateBook(selectedBook);
                }else {
                    Snackbar.make(view, "Title & Author are required!", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getAllBooks().observe(getViewLifecycleOwner(),
                new Observer<List<Book>>() {
                    @Override
                    public void onChanged(List<Book> books) {

                    }
                });
        viewModel.getSelectedBook().observe(getViewLifecycleOwner(),
                new Observer<List<Book>>() {
                    @Override
                    public void onChanged(@Nullable final List<Book> books) {
                        Log.i("DetailTwo", "yes");

                        if(books.size() > 0) {
                            textTitle.setText(books.get(0).getTitle());
                            textAuthors.setText(books.get(0).getAuthors());
                            textYear.setText(books.get(0).getYear());
                            textGenres.setText(books.get(0).getGenres());
                            textPublisher.setText(books.get(0).getPublisher());
                            selectedBook = books.get(0);
                        }
                    }
                });
    }
}